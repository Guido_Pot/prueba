#include <stdio.h>  // Introduce el manejo de enteros
#include <string.h> // Control de strings

void main()
{
	printf("Size de los datos:\n\r");

	// Inciso A
	printf("char: %d\n\r",(int) sizeof(char));
	printf("int: %d\n\r",(int) sizeof(int));
	printf("long: %d\n\r",(int) sizeof(long));
	printf("short: %d\n\r",(int) sizeof(short));
	printf("float: %d\n\r",(int) sizeof(float));
	printf("double: %d\n\r",(int) sizeof(double));

	// Inciso B
	printf("long long: %d\n\r",(int) sizeof(long long));
	printf("long double: %d\n\r",(int) sizeof(long double));
	return;
}
